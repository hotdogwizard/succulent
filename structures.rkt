
(define version "1.15")
(define prompt (string-append version ">"))

(struct func_racket
  (name
   argument_count    ;; how many allowed arguments, -1 is infinate
   racket_function)) ;; the racket lambda to evaluate


(struct var         (name value)) ;; Variable
(struct const_num   (value))      ;; Number
(struct func_lambda (local-state body passed-arguments lexical-state)) ;; Lambda function
(struct arg         (pairs))
(struct statement   (fun args))   ;; Statement ex: (+ 1 2)
(struct construct   (a b))        ;; Construct (cons)
(struct nothing     ())           ;; Null
(struct atom (name))
(struct symbol_const (name))
(struct macro (name args body))

(define test-state1 (list (var "a" 1) (var "b" 2) (var "c" 3) (var "d" 4) (var "e" 5)))
(define test-state2 (list (var "aa" 11) (var "bb" 22) (var "cc" 33) (var "dd" 44) (var "ee" 55)))

(define (new_variable name [value (nothing)])
  ;; Variable inizializer
  (var name value))

(define (new_macro name arguments body)
  (macro name arguments body))

(define (new_lambda lexical-state local-state body [passed-arguments (nothing)])
  ;; Lambda inizializer
  (func_lambda local-state body passed-arguments lexical-state))

(define (new_statement fun args)
  ;; Statement inizializer
  (statement fun args))
