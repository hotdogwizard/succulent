;; #lang racket
(include "parsing.rkt")

;; One may edit this software under the Gplv2 (or later) version

(define eval_debug #f)
(define get_value_debug #f)
(define statement_debug #f)
(define lambda_eval_debug #f)
(define create_lambda_debug #f)

(define (build_list a)
  (if (null? a) (nothing)
      (construct (car a) (build_list (cdr a)))))

(define (destroy_list x)
  (if (nothing? x) '()
      (cons (construct-a x) (destroy_list (construct-b x)))))

(define (dump_state x)
  (for-each (λ (a) (if (not (null? a))
                       (dump a) (printf "\n"))
                       (printf "\n"))
            x))

(define (file2code file_name)
  (map (lambda (expr)
         (list->string
          (apply append (map (lambda (i)
                               (string->list i))
                             (cdr (string-split expr "\n"))))))
         
       (string-split (file->string file_name) "#")))

  
(define (type_eq a b)
  (or  (and (atom? a)
            (atom? b))

       (and (statement? a)
            (statement? b))

       (and (var? a)
            (var? b))

       (and (const_num? a)
            (const_num? b))

       (and (func_racket? a)
            (func_racket? b))
       
       (and (func_lambda? a)
            (func_lambda? b))))
 
(define (uni_get_name obj)
  (cond ((atom? obj)        (atom-name obj))
        ((statement? obj)   (uni_get_name (statement-fun obj)))
        ((var? obj)         (var-name obj))
        ((macro? obj)       (macro-name obj))
        ((symbol_const? obj) (symbol_const-name obj))
        ((func_racket? obj) (func_racket-name obj))
        (else (printf "ERROR: Getting name from global state\n"))))



(define (without name state)
  (filter (λ (obj) (not (equal? (uni_get_name obj) name)))
          state))

  
(define (add_global_state a)
  (set! global_state
        (push (force_list a) (without (uni_get_name a) global_state))))

(define global_state
  (stack (func_racket "+" ;; const_num + n -> const_num
                      -1
                      (lambda a
                        ;; Create a constant number of the sum of all a
                        (const_num (apply + (map (lambda (v) (const_num-value v)) a)))))

         (func_racket "^" ;; const_num ^ n -> const_num
                      -1
                      (lambda (a b)
                        ;; Create a constant number of the sum of all a
                        (const_num (expt (const_num-value a) (const_num-value b)))))
         
         (func_racket "ret" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        
                        a))

         (func_racket "newline" ;; const_num * n -> const_num
                      1
                      (lambda ()
                        (printf "\n")))

         (func_racket "tab" ;; const_num * n -> const_num
                      1
                      (lambda ()
                        (printf "\t")))

         (func_racket "space" ;; const_num * n -> const_num
                      1
                      (lambda ()
                        (printf " ")))


         (func_racket "file" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        
                        a))

         (func_racket "macro" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        
                        a))
         
         (func_racket "eval" ;; const_num * n -> const_num
                      1
                      (lambda (a) a))
         
         (func_racket "var" ;; const_num * n -> const_num
                      2
                      (lambda (a b)
                        (add_global_state (var (uni_get_name a) b))))
         


         (func_racket "log" ;; const_num * n -> const_num
                      1
                      (lambda a
                        (for-each (lambda (v) (dump v) (printf " ")) a) (newline)
                        (if (= 1 (length a)) (car a)
                            (build_list a))))

         (func_racket "sqrt" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        (const_num (sqrt (const_num-value a)))))

         (func_racket "=" ;; const_num * n -> const_num
                      2
                      (lambda (a b)
                        (if (= (const_num-value a) (const_num-value b))
                            (const_num 1)
                            (const_num 0))))

         (func_racket "and" ;; const_num * n -> const_num
                      2
                      (lambda (a b)
                        (if (and (= (const_num-value a) 1)  (= (const_num-value b) 1))
                            (const_num 1)
                            (const_num 0))))

         (func_racket "or" ;; const_num * n -> const_num
                      2
                      (lambda (a b)
                        (if (or (= (const_num-value a) 1)  (= (const_num-value b) 1))
                            (const_num 1)
                            (const_num 0))))

         (func_racket ">" ;; const_num * n -> const_num
                      2
                      (lambda (a b)
                        (if (> (const_num-value a)  (const_num-value b))
                            (const_num 1)
                            (const_num 0))))

         (func_racket "!" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        (if (= (const_num-value a) 0)
                            (const_num 1)
                            (const_num 0))))

         (func_racket "%" ;; const_num * n -> const_num
                      2
                      (lambda (a b) (const_num (modulo (const_num-value a) (const_num-value b)))))

         


         (func_racket "if" ;; const_num * n -> const_num
                      3
                      (lambda (a b c)
                        (if (= (const_num-value a) 1)
                            b
                            c)))

         (func_racket "teq" ;; const_num * n -> const_num
                      1
                      (lambda (a b)
                        (if (type_eq a b) (const_num 1) (const_num 0))))
         (func_racket "sym" 1 (lambda (x) x))
         
         (func_racket "dump" ;; const_num * n -> const_num
                      1
                      (lambda (a)
                        
                        a))
         (func_racket "save" ;; const_num * n -> const_num
                      1
                      (lambda (a b)
                        (set! global_state
                              (push global_state
                                    (stack (func_racket a 1 (lambda () b)))))))
         
         (func_racket "print" ;; const_num * n -> const_num
                      1
                      (lambda a (for-each (lambda (v) (dump v)) a)))


         (func_racket "apply" ;; const_num * n -> const_num
                      -1
                      (lambda a
                        (evaluate (statement (car a) (destroy_list (car (cdr a)))) global_state)))

         (func_racket "list" ;; const_num * n -> const_num
                      -1
                      (lambda a (build_list a)))
         
         (func_racket "/" ;; const_num / n -> const_num
                      -1
                      (lambda (a b)
                        ;; Create a constant number of the sum of all a
                        (const_num (/ (const_num-value a) (const_num-value b)))))

         (func_racket "\\" ;; n \ const_num -> const_num
                      -1
                      (lambda (a b)
                        ;; Create a constant number of the sum of all a
                        (const_num (/ (const_num-value b) (const_num-value a)))))


         (func_racket "*" ;; const_num * n -> const_num
                      -1
                      (lambda a
                        ;; Create a constant number of the product of all a
                        (const_num (apply * (map (lambda (v) (const_num-value v)) a)))))

         
         (func_racket "1/" ;; 1 / const_num -> const_num
                      1
                      (lambda (a)
                        ;; Return a numbers inverse 1/n
                        (const_num (/ 1 (const_num-value a)))))

         (func_racket "cons" ;; something -> construct
                      2 
                      (lambda (a b)
                        ;; Cons
                        (construct a b)))


         (func_racket "car" ;; construct -> construct_a 
                      1 
                      (lambda (a)
                        ;; Car
                        (construct-a a)))

         (func_racket "pass" ;; construct -> construct_a 
                      1
                      (lambda a (const_num (apply (eval (string->symbol (atom-name (car a))))
                                                  (map (lambda (v) (const_num-value v)) (cdr a))))))
                       


         (func_racket "cdr" ;; construct -> construct_b
                      1 
                      (lambda (a)
                        ;; Cdr
                        (construct-b a)))
         (var "pi" (const_num pi))
         (var "e" (const_num (exp 1)))
         (var "NIL" (nothing))))


(define (dump x)
  (cond ((statement? x)
         (begin (printf "(")
                (dump (statement-fun x))
                (printf " ")
                (dump (statement-args x))
                (printf ")")))
         
        ((atom? x) (printf (atom-name x)))

        ((func_lambda? x)
         (begin (printf " (lambda ")
                (dump (func_lambda-local-state x))
                (printf " ")
                (dump (func_lambda-body x))
                (printf ")")))

        ((macro? x)
         (begin (printf " (macro (")
                (printf (macro-name x))
                (printf " ")
                (dump (macro-args x))
                (printf ") ")
                (dump (macro-body x))
                (printf ") ")
                ))

        ((construct? x) (begin (printf "(")
                               (dump (construct-a x))
                               (printf " . ")
                               (dump (construct-b x))
                               (printf ")")))

        ((const_num? x) (printf (number->string (const_num-value x))))

        ((nothing? x) (printf "NIL"))

        ((var? x) (begin
                    (printf "(:= ")
                    (printf (var-name x))
                    (printf " ")
                    (dump (var-value x))
                    (printf ")")))

        ((func_racket? x) (printf (func_racket-name x)))
        ((list? x)
         (begin  (for-each (lambda (v) (dump v) (printf " ")) x)))
        ((pair? x)
         (begin (printf "[")
                (dump (car x))
                (printf " . ")
                (dump (cdr x))
                (printf "]")))

        (else (print x))))
                

        



(define (evaluate statement-expr states)            ;; Evaluate expression 
  (cond ((or (nothing? statement-expr)
             (null? statement-expr))
         (begin (if eval_debug (printf "DEBUG: Returning nothing\n") (void))
                (nothing)))                         ;; Nothing? -> Nothing

        ((evaluated_argument? statement-expr)
         (begin (if eval_debug (begin (printf "DEBUG: Evaluated constant: ") (dump statement-expr) (newline)) (void))
                (let ((value  (get_value statement-expr states))) ;; No need for complex evaluation
                  (cond ((statement? value) (evaluate value states))
                        ((var? value) (evaluate (var-value value) states))
                        (else value)))))

        ((var? statement-expr) (evaluate (var-value statement-expr) states))
        (else (let* ((fun      (statement-fun statement-expr))  ;; Function
                     (args     (statement-args statement-expr)) ;; Evaluated_Argument?
                     (find_fun (if (atom? fun) (get_value fun states) ;; then look for it in state
                                   fun)))
                (begin ;;(if eval_debug (begin (printf "\nDEBUG: iteration, find_fun = ") (dump find_fun) (newline)) (void))
                       
                       (cond ((equal? find_fun 'error-variable-undeclared)
                              ;; Undeclared error checking
                              (if eval_debug (begin (printf "ERROR: Unable to find the given function.\n")
                                                    (dump find_fun))
                                  (void)))
                             
                             ((and (statement? fun) ;; Is lambda?
                                   (equal? (atom-name (statement-fun fun)) "lambda"))

                              (begin (if eval_debug (printf "DEBUG: Found Lambda\n") (void))
                                     (evaluate (statement (lambda_create fun states) args) states)))
                             
                             ((var? find_fun)
                              (begin (if eval_debug (printf "DEBUG: Replacing symbol with lambda \n") (void))
                                     (evaluate (statement (var-value find_fun) args) states)))

                             ((and (not (or (func_lambda? find_fun)
                                            (func_racket? find_fun)))
                                   (statement? fun)
                                   (atom? (statement-fun fun))
                                   (string? (atom-name (statement-fun fun)))
                                   (string=? (atom-name (statement-fun fun)) "lambda"))

                              (if (inner_statement? find_fun)
                                  (begin (if eval_debug (printf "DEBUG: Statement found, evaluating.\n") (void))
                                         (create_inner_statement find_fun states))
                                  (if eval_debug (begin (printf "ERROR: Strange function \n")
                                                        (dump find_fun)) (void))))
                             
                             ;;(if (equal? find_fun 'nothing) (nothing)
                             ;;    (begin (printf "ERROR: Not a valid function\n")
                             ;;           (dump find_fun)))))
                             
                             ((func_lambda? fun)
                              (begin (if eval_debug (printf "DEBUG: Evaluating lambda \n") (void))
                                     (lambda_eval fun args states)))

                             ((macro? find_fun)
                              (begin (if eval_debug (printf "DEBUG: Evaluating macro \n") (void))
                                     (macro_eval find_fun args states)))

                             ((func_lambda? find_fun)
                              (begin (if eval_debug (printf "DEBUG: Evaluating stored lambda \n") (void))
                                     (lambda_eval find_fun args states)))
                             
                             ((func_racket? find_fun)
                              (begin (if eval_debug
                                         (begin (printf "DEBUG: Evaluating function: ")
                                                (dump find_fun)
                                                (printf " with input: ")
                                                (dump args)
                                                (newline))
                                         (void))
                                     (racket_eval find_fun args states)))
                             (else (begin (printf "ERROR: evaluation")
                                          (dump find_fun)))))))))



(define (good_ret x)
  (if (= (length x) 1) (car x) x))


(define (get_value thing states)
  ;; Get a value from states and return it,
  ;; if not found simply return the atom.
  ;; If the thing is a statement, evaluate it first.
  ;; When everything is either an atom, number, lambda or a racket function.
  ;; then return it, if error the given value in it's pure form is returned.
  (if (symbol_const? thing) (symbol_const-name thing)
      (good_ret (map (λ (value)
                       ;; Convert to searchable state
                       (let* ((value_as_atom
                               (cond ((atom? value)    value)
                                     ((var? value)     (atom (var-name value)))
                                     ((nothing? value) (atom ""))))
                              
                              ;; Look for a variable in current state
                              (result (if (not (void? value_as_atom))
                                          (begin (if get_value_debug
                                                     (begin (printf "DEBUG: Looking for ")
                                                            (dump value_as_atom)) (void))
                                                 (find_value value_as_atom states))
                                          ;; Look for value in state
                                          'void)))
                         
                         (if (not (symbol? result))
                             (if get_value_debug
                                 (begin (printf " found ")
                                        (dump result)
                                        (newline)
                                        result)
                                 result) ;; Found something
                             (begin (begin (if get_value_debug
                                               (begin (printf "DEBUG: value returns: ")
                                                      (dump value)
                                                      (newline))
                                               (void))
                                           value)))))
                     (force_list thing)))))


(define (get_arg args index)
  (list-ref args index))

(define (racket_eval racket_fun arguments states)
  (cond ((string=? (func_racket-name racket_fun) "dump") (dump_state states))

        ((string=? (func_racket-name racket_fun) "if")
         (if (equal? 1 (const_num-value (evaluate (get_arg arguments 0) states)))
             (evaluate (get_arg arguments 1) states)
             (evaluate (get_arg arguments 2) states)))
        
        ((string=? (func_racket-name racket_fun) "save")
         (apply (func_racket-racket_function racket_fun) arguments))

        ((string=? (func_racket-name racket_fun) "sym")
         ;;(symbol_const (get_arg arguments 0)))
         (get_arg arguments 0))

        ((string=? (func_racket-name racket_fun) "eval")
         (evaluate (evaluate (delist arguments) states) states))

        ((string=? (func_racket-name racket_fun) "macro")
         (let* ((nameandargs (get_arg arguments 0))
                (name (atom-name (statement-fun nameandargs)))
                (args (statement-args nameandargs))
                (body (get_arg arguments 1)))
           
           (add_global_state (new_macro name args body))))

        ((string=? (func_racket-name racket_fun) "file")
         (map (lambda (e) (evaluate (create_statement (car (deep-parse e)) states) states))
              (file2code (atom-name (get_arg arguments 0)))))

        
        ((string=? (func_racket-name racket_fun) "let")
         (let ((vars (map (lambda (v)
                            (var (atom-name (statement-fun v))
                                 (delist (statement-args v))))
                          (append (force_list (statement-args (car arguments)))
                                  (force_list (statement-fun (car arguments))))))
               (body (car (cdr arguments))))
           (evaluate body (push vars states))))
         
        ((let*  ((set_args (force_list (get_value arguments states)))
                 (eval_args (map (lambda (value) (evaluate value states)) set_args)))
           (apply (func_racket-racket_function racket_fun) eval_args)))))

(define (evaluated_argument? a)
  (or (atom? a)
      (const_num? a)
      (func_lambda? a)
      (func_racket? a)))

(define (inner_statement? test)
  (if (atom? test) (printf "ERROR: Undefined function\n")
      (if (not (evaluated_argument? test)) #f
          (if (null? (cdr test)) #t
              (inner_statement? (cdr test))))))
  
(define (create_inner_statement deep_parsed_list states)
  (statement (car deep_parsed_list) (cdr deep_parsed_list)))

(define (force_list a)
  (if (list? a) a (list a)))


(define (create_statement deep_parsed_list states)
  (cond ((evaluated_argument? deep_parsed_list) deep_parsed_list)
        
        ((not (list? deep_parsed_list)) (get_value deep_parsed_list states))
        
        ((and (string? (car deep_parsed_list))
              (string=? "" (car deep_parsed_list))) (nothing))
        
        (else (let ((function (car deep_parsed_list))
                    (arguments (cdr (force_list deep_parsed_list))))
                
                (begin (cond (statement_debug (dump function) (printf ", ") (dump arguments) (newline)))
                  (statement 
                   (if (list? function) (create_statement function states) function)
                   (map (lambda (argument)
                          (if (evaluated_argument? argument)
                              argument
                              (if (inner_statement? argument)
                                  (create_inner_statement argument states)
                                  (create_statement argument states))))
                        arguments)))))))


(define (find_value atom state) 
  ;; atom must be a const_num or variable, states is a list of state
  (let ((found_value
         (find (λ (value)
                 (string=?
                  (if (var? value)
                      (var-name value)          ;; Compare with variable name
                      (if (macro? value)
                          (macro-name value)
                          (if (func_racket? value)
                              (func_racket-name value)
                              (printf "ERROR: Comparing between wrong datatypes\n"))))
                  (atom-name atom)))
               state)))

    (if (symbol? found_value)
        atom           ;; Value not in state
        found_value))) ;; Value in state

(define (delist a)
  (if (list? a) (car a) a))



(define (statement2list a)
  (cond ((list? a) a)
        ((statement? a) (append (force_list (statement-fun a)) (force_list (statement-args a))))
        (else (printf "ERROR: Weird argument"))))


(define (lambda_create lambda_unfinished_expr state)
  (if create_lambda_debug (printf "DEBUG: Creating Lambda expr\n") (void))

  (let ((args (statement2list (car (statement-args lambda_unfinished_expr))))
        (body (force_list (cdr (statement-args lambda_unfinished_expr)))))
    (new_lambda state args body)))


(define (macro_eval mac args states)
  (let ((matched_args (bind-pargs2vars (macro-args mac) args))) ;; Match passed arguments to args
    (evaluate (macro-body mac)
              (append matched_args
                      states))))
    


  
(define (lambda_eval lambda_expr passed_arguments states)
  ;; Lambda expr (lambda), passed_arguments (list)
  ;;
  ;; Evaluate a lambda expression

  (cond (lambda_eval_debug ;; Debugging print
         (printf "DEBUG: Lambda local state:  ") (dump (func_lambda-local-state lambda_expr))
         (printf "\nDEBUG: Lambda passed arguments:  ") (dump passed_arguments)
         (printf "\n")))
  
  (let* ((visable_variables (func_lambda-lexical-state lambda_expr))             ;; What vars are visable
         (set_args (force_list (get_value passed_arguments visable_variables)))  ;; Search for their value
         (eval_args (map (lambda (value) (evaluate value states)) set_args))     ;; Evaluate them
         (local_variables (bind-pargs2vars (func_lambda-local-state lambda_expr) ;; Bind them
                                           eval_args)))
      
      (begin (cond (lambda_eval_debug ;; Debugging print
                    (printf "DEBUG: Evaluating lambda function with ")
                    (dump local_variables)
                    (newline)))
             ;; Evaluate every argument in order after the arguments
      ;; last-value being the return value.
      (define (iter carry last_value) 
        (if (null? (cdr carry))
            (evaluate (car carry)
                      (append (force_list local_variables)
                              (force_list visable_variables))) ;; * Return *
            (iter (cdr carry)              ;; * Next val *
                  (evaluate (car carry)
                            (append (force_list local_variables)
                                    (force_list visable_variables))))))
      
      (iter (func_lambda-body lambda_expr) (nothing)))))

  
  





;; A lambda expression should contain all
;; of the outside state. hence lexical state
;; could be used.
(define (assign lambda_expr lexical_scope)
  ;; Assign the evaluated_argument?s put on a lambda expr
  ;; into its localstate, also add the visible scope
  (new_lambda (bind-pargs2vars (func_lambda-local-state lambda_expr)
                               (func_lambda-passed-arguments lambda_expr))
              (func_lambda-body lambda_expr)
              (func_lambda-passed-arguments lambda_expr)
              lexical_scope))

